import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class TestSoftaria
{
    private static final String NAME_OLD_TABLE = "HashTableOld.csv";
    private static final String NAME_NEW_TABLE = "HashTableNew.csv";

    private static Map tableOfChangedNotes;
    private static Map tableOfDeletedNotes;
    private static Map tableOfAddedNotes;


    public static void main(String[] args) throws IOException
    {
        Map<String, String> hashMapOld = CvsToMap(NAME_OLD_TABLE);
        Map<String, String> hashMapNew = CvsToMap(NAME_NEW_TABLE);

        TableProcessing(hashMapOld, hashMapNew);
        String letter = WriteLetter();

        System.out.println(letter);
    }

    private static void TableProcessing(Map<String, String> oldTable, Map<String, String> newTable)
    {
        MapDifference tableDifference = Maps.difference(oldTable, newTable);
        tableOfChangedNotes = tableDifference.entriesDiffering();
        tableOfDeletedNotes = tableDifference.entriesOnlyOnLeft();
        tableOfAddedNotes = tableDifference.entriesOnlyOnRight();
    }

    private static String WriteLetter()
    {
        Set<String> changedNotes = tableOfChangedNotes.keySet();
        Set<String> deletedNotes = tableOfDeletedNotes.keySet();
        Set<String> addedNotes = tableOfAddedNotes.keySet();


        String letter = "";

        letter += "Здравствуйте, дорогая и.о. секретаря.\n";
        letter += "За последние сутки во вверенных Вам сайтах произошли следущие изменения:\n";

        letter += WriteListUrl("Изчезли следующие страницы:", deletedNotes);
        letter += WriteListUrl("Появились следующие новые страницы:", addedNotes);
        letter += WriteListUrl("Изменились следующие страницы:", changedNotes);

        letter += "С уважением,\n";
        letter += "автоматизированная система мониторинга.";

        return letter;
    }

    private static String WriteListUrl(String headerText, Set<String> listUrl)
    {
        String text = "";

        if (listUrl.size() > 0)
        {
            text += (headerText + "\n");

            for (String url : listUrl)
            {
                text += (url + "\n");
            }
        }

        return text;
    }

    private static Map<String, String> CvsToMap(String nameFile) throws IOException
    {
        Map<String, String> hashMap = new HashMap<>();
        List<String> fileLines = Files.readAllLines(Paths.get(GetAbsoluteFilePath(nameFile)));

        int i;
        for(i = 0; i < fileLines.size(); i++)
        {
            String[] splitedText = fileLines.get(i).split(";");

            String url = splitedText[0];
            String htmlCode = splitedText[1];

            if(splitedText.length > 2)
            {
                int j;
                for (j = 2; j < splitedText.length; j++)
                {
                    htmlCode += ";" + splitedText[j];
                }
            }

            hashMap.put(url, htmlCode);

        }
        return hashMap;
    }
    private static String GetAbsoluteFilePath(String nameFile)
    {
        URL url = TestSoftaria.class.getClassLoader().getResource(nameFile);
        System.out.println(url);

        //Переводим URL в строку и удаляем "file:/" в начале строки
        String filePath = url.toString().substring(6);
        filePath = filePath.replaceAll("%20", " ");

        return filePath;
    }
}
